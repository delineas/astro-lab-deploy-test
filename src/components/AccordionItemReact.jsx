export default function AccordeonItemReact(props) {
  return (
    <details className="accordeon-item">
      <summary className="accordeon-item__summary">{props.title}</summary>
      {props.children}
    </details>
  )
}